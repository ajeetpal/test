<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TestController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
}); */

/* 
*User rout start from here
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('show-product', 'user\ProductController@showProduct')->name('product');
/* 
*User rout end here
*/

Route::get('test/view', function(){
    return view('testView');
})->name('testView');
Route::match(['get', 'post'],'user-data', 'TestController@testing')->name('getUser');
// Route::view('/login', 'login')->name('login');

Auth::routes();
Auth::routes(['register' => false]);

// Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/user/login')->name('user-login');

Route::get('admin/home', 'Auth\AdminHomeController@index')
    ->name('admin-home');


Route::get('logout', 'TestController@logout')->name('logout');

