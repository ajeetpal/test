<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TestController extends Controller
{
   public function testing(Request $request){
    //   echo $request->name();
    echo "<pre>";
   //  $user = Auth::user();
   $user = $request->user();
    print_r($request->input('name'));
//    var_dump($user);

  /*  if(Auth::check()){
      echo Auth::guard('guard-name');
   } */
   }

   public function logout()
   {
    //   $this->guard()->logout();
    Auth::logout();
      return view('/login');
   }

   /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

}
