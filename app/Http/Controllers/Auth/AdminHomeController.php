<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
// use Illuminate\Support\Facades\App;
use App\model\Product;

class AdminHomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }
    public function index(Request $request)
    {
      if(!(Auth::check())){
         return view('login');
      }
      // echo App::envoirment('local');exit;
      // print_r(config('app.testing'));exit;
      // echo $request->is('admin/home');exit;
      // echo App::environment();exit;
      // $user = DB::select('select * from users where role = ?', ['user']);
      // print_r($user); exit;

      $porducts = DB::table('products')->get();


      // print_r($porducts['']);exit;
                  
      return view('admin.admin-home', ['products'=>$porducts]);
    }
}
