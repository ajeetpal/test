@extends('admin.master1')

<link href="{{asset('auth/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">

@section('title', 'admin-home')

@section('content')
     <!-- Begin Page Content -->
     <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Tables</h1>
          <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below.
              For more information about DataTables, please visit the <a target="_blank"
                  href="https://datatables.net">official DataTables documentation</a>.</p>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
              
              <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
              </div>
              <div class="card-body">
                  <div class="table-responsive">
                      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                          <thead>
                              <tr>
                                  <th>ID</th>
                                  <th>NAME</th>
                                  <th>PRICE</th>
                                  <th>ADDED_AT</th>
                                  <th>UPDATED_AT</th>
                              </tr>
                          </thead>
                          <tfoot>
                              <tr>
                                <th>ID</th>
                                  <th>NAME</th>
                                  <th>PRICE</th>
                                  <th>ADDED_AT</th>
                                  <th>UPDATED_AT</th>
                              </tr>
                          </tfoot>
                          <tbody>
                                @foreach( $products as $product ) 
                                    <tr>
                                        <td>{{$product->id}}</td>
                                        <td>{{$product->name}}</td>
                                        <td>{{$product->price}}</td>
                                        <td>{{$product->created_at}}</td>
                                        <td>{{$product->updated_at}}</td>
                                    </tr>
                                @endforeach
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>

      </div>
      <!-- /.container-fluid -->
@endsection
@section('footerJavascript')
      @parent
         <!-- Page level plugins --> 
        {{-- <script src="{{asset('auth/vendor/datatables/jquery.dataTables.min.js')}}"></script> --}}
        {{-- <script src="{{asset('auth/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script> --}}

      <!-- Page level custom scripts --> 
        {{-- <script src="{{asset('auth/js/demo/datatables-demo.js')}}"></script> --}}
@endsection